module NbString =
  struct
    let safe_index str ch =
      if String.contains str ch then
	    Some (String.index str ch) else None
      
    let safe_index_from str from ch =
      if String.contains_from str from ch then
	    Some (String.index_from str from ch) else None	
      
    let rest str =
      if str = "" then ""
      else String.sub str 1 ((String.length str) - 1)


    let by_start_close str start_char end_char =
      let str_length = String.length str in
      let rec loop curr_idx num_nest start_found start_idx =
        if str = "" then None
        else
          let curr_char = str.[curr_idx] in
          if curr_char = start_char then
            if start_found then
              loop (curr_idx + 1) (num_nest + 1) true start_idx
            else
              loop (curr_idx + 1) (num_nest + 1) true curr_idx
          else if curr_char = end_char then
            if start_found then
              if num_nest <= 1 then
                let end_idx = (curr_idx - start_idx) in
                let str_rest = if end_idx >= (str_length - 1) then "" else String.sub str (end_idx + 1) ((str_length - end_idx)- 1) in
                Some (String.sub str (start_idx + 1) (end_idx - 1) , str_rest)
              else
                loop (curr_idx + 1) (num_nest - 1) start_found start_idx
            else
              loop (curr_idx + 1) num_nest start_found start_idx
          else loop (curr_idx + 1) num_nest start_found start_idx in
      loop 0 0 false 0
      
	let get_nested_string str =
      let start_pos =
	    safe_index str '"' in
      let stop_pos =
	    match start_pos with
	      None -> None
	    | Some st -> safe_index_from str (st + 1) '"' in
      let start_stop = (start_pos, stop_pos) in  
      match start_stop with
	    (None, _) -> None
      | (_, None) -> None
      | (Some start, Some stop) ->
	     let st = start + 1 in
	     Some (String.sub str st (stop - st), String.sub str (stop + 1) (String.length str - (stop + 1)))
         
    let digest_when_in str digestables =
      let max_idx = String.length str in
      let rec loop json digs idx =
	    if idx != max_idx then
	      let curr = String.get json idx in
	      if List.mem curr digs then
		    loop json digs (idx + 1)
	      else
		    (String.sub json 0 idx, String.sub json idx ((String.length json) - idx))
	    else
	      (String.sub json 0 idx, "") in
      loop str digestables 0
      
    let skip_chars str to_skip =
      let max_idx = String.length str in
      let rec loop json to_skip idx =
	    if idx != max_idx then
	      let curr = String.get json idx in
	      if List.mem curr to_skip then
		    loop json to_skip (idx + 1)
	      else
		    String.sub json idx ((String.length json) - idx)
	    else
	      String.sub json idx ((String.length json) - idx) in
      loop str to_skip 0
      
    let find_first f str =
      let length = String.length str in
      let rec loop idx =
        if idx < length then
          let h = String.get str idx in
          if f h then
            Some idx
          else
            loop (idx + 1)
        else
          None
      in
      loop 0
      
      
  end

