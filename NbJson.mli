val jsonstr : string
type jsonType =
  | JSAssoc of (string * jsonType) list
  | JSBool of bool
  | JSFloat of float
  | JSList of jsonType list
  | JSNull
  | JSString of string;;
				 
val get_key : string -> (string * string) option
val get_val : string -> (jsonType * string) option
val get_kv_pair : string -> ((string * jsonType) * string) option
val js_parse : string -> (string * jsonType) list
val js_array : string -> jsonType list

