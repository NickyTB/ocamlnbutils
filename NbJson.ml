
let jsonstr = "{ \"Name\" : \"Lasse\", \"Age\" : 33 , \"MyList\" :  [2 , \"Hej\", 11.44 , { \"InnerAdr\" : { \"StreetZip\" : \"123.33\"}, \"City\" : \"Malmoe\" }, [1, 44, \"Text\"]] , \"Anithyn\" : Null, \"Adress\" : { \"Street\" : \"Lassegatan\"}}"

let number_chars = [ '1' ; '2' ; '3' ; '4' ; '5' ; '6' ; '7' ; '8' ; '9' ; '0' ; '.']

type jsonType =
  | JSAssoc of (string * jsonType) list
  | JSBool of bool
  | JSFloat of float
  | JSList of jsonType list
  | JSNull
  | JSString of string
				 
	
let get_key json =
  if String.contains json '"' then
	NbString.get_nested_string json
  else None

		   
let rec get_val json =
  if json == "" then
	None
  else
	let str = NbString.skip_chars json [ ' ' ; ':' ; '\n' ; '\r' ; '\t'] in
	let frst = String.get str 0 in
	if frst == '"' then NbString.get_nested_string str
						|> function
						  | None -> None
						  | Some (strVal, rest) -> Some (JSString strVal, rest)
	else if frst ==  '[' then NbString.by_start_close str '[' ']' true
							  |> function
								| None -> None
								| Some (arrVal, rest) -> Some (JSList (js_array arrVal), rest)	  
	else if frst == '{' then NbString.by_start_close str '{' '}' false
							 |> function
							   | None -> None
							   | Some (objVal, rest) -> Some (JSAssoc (js_parse objVal), rest)
	else if frst == 'n' || frst == 'N' then Some ( "Null", String.sub str 4 ((String.length str) - 4))
											|> function
											  | None -> None
											  | Some (nullVal, rest) -> Some (JSNull, rest)
	else if frst == 'f' then Some ( "false", String.sub str 5 ((String.length str) - 5))
							 |> function 
							   | None -> None
							   | Some (boolVal, rest) -> Some (JSBool false, rest)
	else if frst == 't' then Some ( "true", String.sub str 4 ((String.length str) - 4))
							 |> function
							   | None -> None
							   | Some (boolVal, rest) -> Some (JSBool true, rest)
	else if List.mem frst number_chars then Some (NbString.digest_when_in str number_chars)
											|> function
											  | None -> None
											  | Some (nrVal, rest) -> Some (JSFloat (float_of_string nrVal), rest)
	else None
and js_array json =
  let rec loop js acc =
	if js = "" then
	  acc
	else
	  let str = NbString.skip_chars js [ ' ' ; ',' ; '\n' ; '\r' ; '\t'] in
	  if str = "" then
		acc
	  else
		let next_val = get_val str in
		match next_val with
		| None -> acc
		| Some (jsType, rest) -> loop rest (jsType::acc) in
  loop json []
and js_parse json =
  let rec loop js acc =
	if js == "" then
	  acc
	else
	  let kv = get_kv_pair js in
	  match kv with
	  | None ->  acc
	  | Some (kv, rest) -> loop rest (kv::acc) in
  loop json []
and get_kv_pair json =
  let key = get_key json in
  match key with
  | None -> None
  | Some (key, rest) -> let value = get_val rest in
						match value with
 						| None -> None
						| Some (value, rest1) -> Some ((key, value), rest1) 




