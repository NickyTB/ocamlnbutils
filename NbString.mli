module NbString :
sig
  val by_start_close : string -> char -> char -> (string * string) option
  val safe_index : string -> char -> int option
  val safe_index_from : string -> int -> char -> int option
  val get_nested_string : string -> (string * string) option
  val digest_when_in : string -> char list -> string * string
  val skip_chars : string -> char list -> string
  val find_first : (char -> bool) -> string -> int option
  val rest : string -> string
end

